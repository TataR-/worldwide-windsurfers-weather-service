


# Worldwide Windsurfer�s Weather Service



## Start the application using command line




Clone repository on your local machine, go to the repository folder 
and run command in your command line ```mvnw spring-boot:run``` on Windows or ``./mvnw spring-boot:run`` on MacOS/Linux. 
Open your browser and in the address bar at the top enter http://localhost:8080/weather then we will get a
response with the best windsurfing location of today's date. If we want to display the best location for 
windsurfing for another day we must provide date in appropriate form which looks like this 
http://localhost:8080/weather?date=yyyy-mm-dd. 



## Description
Worldwide Windsurfer�s Weather Service is a simple application with which we can determine the best place for windsurfing
in one of the following locations:

* Jastarnia (Poland),
* Bridgetown (Barbados),
* Fortaleza (Brazil),
* Pissouri (Cyprus),
* Le Morne (Mauritius).

The application allows us to easily expand the list of places by adding new
places to the array which is embedded in the application. The weather in these 5 places 
is taken from Weatherbit Forecast API (https://www.weatherbit.io/api/weather-forecast-16-day). 
The weather request is sent to the API as soon as the program is started on a separate thread.
This solution is due to the fact that for each location you have to reconnect with api.
You can't download forecasts for multiple locations in a single connection. Data from api are 
downloaded once an hour in case the weather forecast is updated.
