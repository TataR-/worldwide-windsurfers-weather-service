package com.example.Worldwide.Windsurfer.s.Weather.Service;

import java.util.HashMap;

public interface ServiceRunThread {
    HashMap<String, HashMap<String, WeatherTable>> getLocations();
}
