package com.example.Worldwide.Windsurfer.s.Weather.Service;

import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Component
public class RunThread implements ServiceRunThread {

    public HashMap<String, HashMap<String, WeatherTable>> location = new HashMap<>();

    ServiceWeatherTable serviceWeatherTable;

    public RunThread(ServiceWeatherTable serviceWeatherTable) {
        this.serviceWeatherTable = serviceWeatherTable;
    }
    final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    final ScheduledFuture<?> beepHandler =
            scheduler.scheduleAtFixedRate(new Runnable() {
                @SneakyThrows
                @Override
                public void run() {
                    location = serviceWeatherTable.getDataFromApi();
                }
            }, 0, 1, TimeUnit.HOURS);

    public HashMap<String, HashMap<String, WeatherTable>> getLocations() {
        return location;
    }

}
